#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import correlated_values
import pickle
import utils
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--txt', action='store_false',
                    dest='use_pkl', default=False)
parser.add_argument('--pkl', action='store_true',
                    dest='use_pkl', default=False)
parser.add_argument('--dump-to-pkl', action='store_true',
                    dest='dump_to_pkl', default=False)
parser.add_argument('--one-par', action='store_true',
                    dest='one_param_fit', default=False)
parser.add_argument('--two-par', action='store_false',
                    dest='one_param_fit', default=False)
parser.add_argument('dirpaths', metavar='dir', type=str, nargs='+')
args = parser.parse_args()


def txtToPickle(filepath):
    params = utils.get_parameters(filepath)
    data = utils.load_data(filepath, discard_last=1)
    data = np.reshape(data, (len(data), params['Nt'], params['m_max']))
    data = np.swapaxes(data, 1, 2)  # 3D array, axes are t_MC, k, tau

    if args.dump_to_pkl:
        with open(filepath.replace('txt', 'pkl'), 'wb') as output_file:
            pickle.dump(data, output_file)

    return data


def datafile_to_dispersion(filepath, skip_therm, plot=False):
    # Caricamento dati
    if filepath.endswith('.txt'):
        data = txtToPickle(filepath)
    elif filepath.endswith('.pkl'):
        with open(filepath, 'rb') as pickle_file:
            data = pickle.load(pickle_file)
    else:
        print("Unrecognized input file extension")

    data = data[skip_therm:]
    params = utils.get_parameters(filepath.replace('.pkl', '.txt'))

    # Media e blocking delle correlazioni
    CorrN = np.mean(data, axis=0)

    # np.apply_along_axis(lambda x: utils.find_blocksize(x, 1, 1, params['mhat']), 0, data)
    CorrS = np.apply_along_axis(lambda x: utils.blocking(x, 10), 0, data)

    # Fit della correlazione a fisso impulso
    if args.one_param_fit:
        def corr_fit(x, a):
            return np.exp(-a*x) / (2 * a)
    else:
        def corr_fit(x, a, b):
            return np.exp(-a*x) / (2 * b)


    fit_start = 0
    fit_stop = int(CorrN.shape[1]/3)

    energy = []
    energy_err = []

    for k_idx in range(CorrN.shape[0]):
        # k = 2 * np.pi * k_idx / params['Ns']
        # x = range(fit_start, fit_stop)
        # yn = CorrN[k_idx][fit_start:fit_stop]
        # ys = CorrS[k_idx][fit_start:fit_stop]

        # par0 = np.sqrt(params['mhat']**2 + k**2)
        # if args.one_param_fit:
        #     p0 = [par0]
        # else:
        #     p0 = [par0,par0]

        # opt, cov = curve_fit(
        #     corr_fit, x, yn, sigma=ys,
        #     absolute_sigma=True, p0=p0
        # )

        k = 2 * np.pi * k_idx / params['Ns']

        # fit preliminare
        fit_start = 0
        fit_stop = int(CorrN.shape[1]/4)

        par0 = np.sqrt(params['mhat']**2 + k**2)
        if args.one_param_fit:
            p0 = [par0]
        else:
            p0 = [par0, par0]

        x = range(fit_start, fit_stop)
        yn = CorrN[k_idx][fit_start:fit_stop]
        ys = CorrS[k_idx][fit_start:fit_stop]
        opt, cov = curve_fit(corr_fit, x, yn, sigma=ys, absolute_sigma=True, p0=p0)

        # fit preciso
        fit_stop = int(3/opt[0])
        x = range(fit_start, fit_stop)
        yn = CorrN[k_idx][fit_start:fit_stop]
        ys = CorrS[k_idx][fit_start:fit_stop]
        opt, cov = curve_fit(corr_fit, x, yn, sigma=ys, absolute_sigma=True, p0=opt)

        chi2 = utils.chisq(x, yn, ys, corr_fit, opt)
        print(f'mhat = {params["mhat"]}, k = {k}, chi2 / ndof = {chi2 / (len(yn) - len(opt)):.3f}')

        energy.append(opt[0])
        energy_err.append(np.sqrt(cov[0][0]))

        if plot:
            x_plot = np.linspace(0, len(CorrN[k_idx]), num=500)
            plt.plot(2*[fit_stop], [min(CorrN[k_idx]), max(CorrN[k_idx])])
            plt.plot(x_plot, corr_fit(x_plot, *opt), color='black', linewidth='0.5')
            plt.errorbar(range(len(CorrN[k_idx])), CorrN[k_idx], yerr=CorrS[k_idx], linestyle='')
            plt.title(rf'$ \hat m = {params["mhat"]}, N_s = {params["Ns"]}, N_t = {params["Nt"]}, k = {k} $')
            plt.xlabel(r'$ \tau $')
            plt.ylabel(r'$ \langle \tilde\phi(k, 0) \tilde\phi(k, \tau) \rangle $')

            plt.show()

    energy = np.array(energy)
    energy_err = np.array(energy_err)
    momentum = 2*np.pi * np.arange(CorrN.shape[0]) / params["Ns"]
    return momentum, energy, energy_err, params['mhat']


def fit_gap(momentum, energy, energy_err, mhat, plot=False):
    max_momentum_fit = 0.3  # 0.4 << 1

    def dispersion(k, gap):
        return np.sqrt(gap**2 + k**2)

    mask = momentum < max_momentum_fit
    x = momentum[mask]
    yn = energy[mask]
    ys = energy_err[mask]

    opt, cov = curve_fit(dispersion, x, yn, sigma=ys, p0=(mhat), absolute_sigma=True)

    chi2 = utils.chisq(x, yn, ys, dispersion, opt)
    print(f'Dispersione: mhat = {mhat}, chi2 / ndof = {chi2 / (len(yn) - len(opt)):.3f}')

    gap = correlated_values(opt, cov)[0]

    if plot:
        x_plot = np.linspace(momentum.min(), momentum.max(), num=500)
        plt.vlines(max_momentum_fit, energy.min()/mhat, energy.max()/mhat,
                   color='green', linestyles='dashed')
        plt.plot(x_plot, dispersion(x_plot, *opt)/mhat, color='red')
        plt.errorbar(momentum, energy/mhat, yerr=energy_err/mhat, fmt=' .b')
        plt.errorbar(0, gap.n/mhat, yerr=gap.s/mhat, color='orange')
        plt.xlabel('$ k $')
        plt.ylabel(r'$ E(k)/\hat{m}_\mathrm{sim} $')
        plt.show()

    return gap / mhat


if __name__ == "__main__":
    if args.use_pkl:
        extension = 'pkl'
    else:
        extension = 'txt'

    mhats = []
    gapsN = []
    gapsS = []

    # plt.style.use('style.yml')

    for dirp in args.dirpaths:
        files = filter(lambda s: s.endswith(extension)
                       and s.startswith('spec'), os.listdir(dirp))
        for f in files:
            filepath = os.path.join(dirp, f)
            print(filepath)
            momentum, energy, energy_err, mhat = datafile_to_dispersion(filepath, 5, plot=False)
            gap = fit_gap(momentum, energy, energy_err, mhat, plot=False)
            mhats.append(mhat)
            gapsN.append(gap.n)
            gapsS.append(gap.s)

    plt.show()
    mhats = np.array(mhats)
    gapsN = np.array(gapsN)
    gapsS = np.array(gapsS)

    idx = np.argsort(mhats)

    def parabola(x, a, b):
        return a + b*x**2

    opt, cov = curve_fit(parabola, mhats, gapsN, sigma=gapsS, absolute_sigma=True)

    chi2 = utils.chisq(mhats, gapsN, gapsS, parabola, opt)
    print(f'chi2 / ndof = {chi2 / (len(mhats) - len(opt))}')

    print(correlated_values(opt, cov))

    np.savetxt('spectrum-fit2.txt', np.array([mhats, gapsN, gapsS]).transpose(), header='mhat \t mfit/msim \t err')

    x_plot = np.linspace(0, mhats.max(), num=500)

    plt.plot(x_plot, parabola(x_plot, *opt), color='red')
    plt.errorbar(0, opt[0], yerr=np.sqrt(cov[0][0]), color='orange')

    plt.errorbar(mhats[idx], gapsN[idx], yerr=gapsS[idx],
                 linestyle='', marker='.', color='blue')
    plt.xlabel(r'$\hat{m}_{sim}$')
    plt.ylabel(r'$\hat{m}_{fit} / \hat{m}_{sim}$')
    # plt.savefig('imgs/spectrum-fit2.png')
    # plt.close()
    plt.show()
