#!/usr/bin/env python

import argparse
import os

parser = argparse.ArgumentParser(
    description='Lancia in sequenza gli script per il calcolo dello spettro'
)
parser.add_argument(
    '--input-directory', '-i', action='store', dest='input_dir',
    help='Input directoring with Monte Carlo history of correlators'
)
parser.add_argument(
    '--prefix', '-p', action='store', dest='prefix',
    help='Prefix used for output files, directiories and images'
)
args = parser.parse_args()

DIR = args.input_dir
PREFIX = args.prefix
OUT_DIR = os.path.join('saves', PREFIX)
OUT_DIR_IMG = os.path.join('imgs', PREFIX)

os.makedirs(OUT_DIR, exist_ok=True)
os.makedirs(OUT_DIR_IMG, exist_ok=True)

os.system(f'./spectrum_correlator.py {DIR} {OUT_DIR_IMG} {OUT_DIR}')
os.system(f'./spectrum_energy.py {OUT_DIR} {OUT_DIR_IMG} saves/{PREFIX}_mhat.txt')
os.system(f'./spectrum_mhat.py saves/{PREFIX}_mhat.txt {OUT_DIR_IMG}')
