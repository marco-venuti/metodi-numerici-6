#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import correlated_values
import utils

plt.style.use('style.yml')


def parabola(x, a, b):
    return a + b*x**2


mhats, gapsN, gapsS = np.loadtxt(sys.argv[1], unpack=True)

opt, cov = curve_fit(parabola, mhats, gapsN, sigma=gapsS, absolute_sigma=True)

chi2 = utils.chisq(mhats, gapsN, gapsS, parabola, opt)
a, b = correlated_values(opt, cov)

print(f'Fit f(x) = a + b x^2 \t a = {a:.2u} \t b = {b:.2u} \t chi2 / ndof = {chi2} / {(len(mhats) - len(opt))}')

x_plot = np.linspace(0, mhats.max(), num=500)

plt.plot(x_plot, parabola(x_plot, *opt), color='red')
plt.errorbar(0, opt[0], yerr=np.sqrt(cov[0][0]), color='orange')

plt.errorbar(mhats, gapsN, yerr=gapsS, linestyle='', marker='.', color='blue', markersize=2)
plt.xlabel(r'$\hat{m}$')
plt.ylabel(r'$\hat{m}_\mathrm{fit} / \hat{m}$')
plt.savefig(f'{sys.argv[2]}/spectrum-fit.pdf')
plt.close()
plt.show()
