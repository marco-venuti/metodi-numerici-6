#!/usr/bin/env python3

import sys
import matplotlib.pyplot as plt

import utils

plt.rcParams['agg.path.chunksize'] = 10000

phi2 = utils.load_data(sys.argv[1])

params = utils.get_parameters(sys.argv[1])
title = f'$N_t = {params["Nt"]}, N_s = {params["Ns"]}, \\hat{{m}} = {params["mhat"]}$'

# Montecarlo history
plt.plot(range(len(phi2)), phi2, color='black', linewidth=0.2)
plt.xlabel('$t_{MC}$')
plt.ylabel(r'$\phi^{2}$')
plt.title(title)
plt.show()

# Montecarlo correlations
skip_therm = 50
corr_phi2 = utils.connected_time_correlation(
    phi2[skip_therm:], int(len(phi2)/10), normalized=True
)

plt.plot(range(len(corr_phi2)), corr_phi2, color='black')
plt.xlabel("$t_{MC}$")
plt.ylabel(r"$C_{\phi^2}$")
plt.title(title)
plt.show()
