import numpy as np
import pandas as pd
import yaml
import re
import matplotlib.pyplot as plt


def chisq(xs: (np.array), ys: (np.array), yerrs: (np.array), model, fit_params):
    return np.sum(((ys - model(xs, *fit_params)) / yerrs) ** 2)


def filename_to_params(s):
    match = re.match('.*-([0-9\.]+)-([0-9]+)-([0-9]+)\.txt', s)
    if not match:
        raise Exception('AAAAA')

    return (
        float(match.group(1)), # mhat
        int(float(match.group(2))), # Ns
        int(float(match.group(3))) # Nt
    )


def get_parameters(filename: str) -> dict:
    """Get parameters from first line of a data file. Parameters are
    expected to be present in the first line in form
    # param1: value1, param2: value2, ...
    """

    with open(filename, "r") as f:
        first_line = f.readline()

    if first_line[0] == '#':
        return yaml.safe_load(first_line.strip('# ').replace(', ', '\n'))

    return None


def load_data(file_name: str, dtype=None, discard_last=0) -> np.array:
    data = pd.read_csv(file_name, comment='#', header=None, sep=' ')
    if discard_last != 0:
        data = data.iloc[:, :-discard_last]  # remove trailing nan column
    if dtype is None:
        return np.array(data)
    else:
        return np.array(data, dtype=dtype)


def stdMean(data: np.array) -> float:
    return data.std(ddof=1)/np.sqrt(len(data))


def find_blocksize(data, max_frac, step, label):
    max_block_size = int(len(data)/max_frac)
    blockSizes = range(1, max_block_size, step)
    stds = []

    for blockSize in blockSizes:
        stds.append(blocking(data, blockSize))

    plt.plot(blockSizes, stds / stds[0], label=label)
    plt.ylim(0)
    plt.legend()
    plt.show()


def blocking(data, blockSize):
    Nblocks = int(len(data)/blockSize)

    stop = blockSize*Nblocks
    blocks = np.reshape(data[:stop], (Nblocks, blockSize))
    avgs = np.mean(blocks, axis=1)

    return stdMean(avgs) if Nblocks > 1 else 0


def connected_time_correlation(x: np.array, max_time, normalized=False) -> np.array:
    if max_time > len(x) - 1:
        raise IndexError

    x_mean = x.mean()

    C = np.array(
        [(x * x).mean() - x_mean**2] +
        [(x[:-k] * x[k:]).mean() - x_mean**2 for k in range(1, max_time)]
    )

    if normalized:
        if C[0] != 0:
            return C / C[0]
        else:
            print("C[0] is zero, returning unnormalized correlations")

    return C
