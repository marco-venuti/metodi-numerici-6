#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import correlated_values
from tqdm import tqdm
from glob import glob
from sys import argv

import utils

plt.style.use('style.yml')

files = sorted(
    glob(f'{argv[1]}/phi2*.txt'),
    key=lambda x: utils.get_parameters(x)["mhat"]
)  # sorta prima in base a mhat

skip_therm = 100
mhats = []
phi2s = []
phi2_errs = []

for f in tqdm(files):
    data = utils.load_data(f).flatten()[skip_therm:]

    mhat, Ns, Nt = utils.filename_to_params(f)
    mhats.append(mhat)

    utils.find_blocksize(data, 7, 1, f'{mhat}')
    plt.title(rf'$\hat{{m}} = {mhat:.3f}$')
    plt.xlabel('block size')
    plt.ylabel(r'$ \sigma_{\langle{\phi^2}\rangle} $')
    plt.savefig(f'imgs/phi2-blocking-{mhat:.3f}.pdf')
    plt.close()

    phi2s.append(data.mean())
    phi2_errs.append(utils.blocking(data, 400))


mhats = np.array(mhats)
phi2s = np.array(phi2s)
phi2_errs = np.array(phi2_errs)


def retta(x, m, q):
    return m*x + q


mask = mhats > 0 #0.0105
x = np.log(mhats[mask])
y_n = phi2s[mask]
y_s = phi2_errs[mask]

opt, cov = curve_fit(retta, x, y_n, sigma=y_s, absolute_sigma=True)

m, q = correlated_values(opt, cov)
print(-2*np.pi * m)

chi2 = (((y_n - retta(x, *opt))/y_s)**2).sum()
ndof = len(x) - 2
print(f'chi2/ndof = {chi2}/{ndof}')

x_plt = np.linspace(mhats[mask].min(), mhats[mask].max(), num=100)

plt.errorbar(mhats, phi2s, yerr=phi2_errs,
             marker='.', markersize=2, linestyle='', color='blue')
plt.plot(x_plt, retta(np.log(x_plt), *opt), color='black')
plt.xscale('log')
plt.xlabel(r'$ \hat m $')
plt.ylabel(r'$ \langle \phi^2 \rangle $')
plt.savefig('imgs/phi2.pdf')
plt.show()

# plt.errorbar(mhats, (phi2s - retta(np.log(mhats), *opt)) / phi2_errs, yerr=len(mhats)*[1], linestyle='', marker='.')
# plt.show()

