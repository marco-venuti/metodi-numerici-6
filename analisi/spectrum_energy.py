#!/usr/bin/env python3

import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import correlated_values

import utils


PLOT_DISPERSION = True
MAX_MOMENTUM_FIT = 0.3


plt.style.use('style.yml')


def fit_gap(momentum, energy, energy_err, mhat):
    max_momentum_fit = MAX_MOMENTUM_FIT

    def dispersion(k, gap):
        return np.sqrt(gap**2 + k**2)

    mask = momentum < max_momentum_fit
    x = momentum[mask]
    yn = energy[mask]
    ys = energy_err[mask]

    opt, cov = curve_fit(dispersion, x, yn, sigma=ys, p0=(mhat), absolute_sigma=True)

    chi2 = utils.chisq(x, yn, ys, dispersion, opt)
    mhat_fit = correlated_values(opt, cov)[0]
    print(f'mhat_sim = {mhat:.2f} \t mhat_fit = {mhat_fit:.1u} \t chi2 / ndof = {chi2:.1f} / {len(yn) - len(opt)}')

    gap = correlated_values(opt, cov)[0]

    if PLOT_DISPERSION:
        x_plot = np.linspace(momentum.min(), momentum.max(), num=500)
        plt.vlines(max_momentum_fit, energy.min()/mhat, energy.max()/mhat,
                   color='green', linestyles='dashed')
        plt.plot(x_plot, dispersion(x_plot, *opt)/mhat, color='red')
        plt.errorbar(momentum, energy/mhat, yerr=energy_err/mhat, fmt=' .b', markersize='1')
        plt.errorbar(0, gap.n/mhat, yerr=gap.s/mhat, color='orange')
        plt.xlabel('$ k $')
        plt.ylabel(r'$ E(k)/\hat{m} $')
        plt.savefig(f'{sys.argv[2]}/energy-{mhat}.pdf')
        # plt.show()
        plt.close()

    return gap / mhat


mhats = []
gapsN = []
gapsS = []

for f in os.listdir(sys.argv[1]):
    filepath = os.path.join(sys.argv[1], f)

    mhat = utils.get_parameters(filepath)['mhat']
    Ns = utils.get_parameters(filepath)['Ns']
    momentum, energy, energy_err = np.loadtxt(filepath, unpack=True)

    gap = fit_gap(momentum, energy, energy_err, mhat)
    mhats.append(mhat)
    gapsN.append(gap.n)
    gapsS.append(gap.s)


idx = np.argsort(mhats)
np.savetxt(
    sys.argv[3],
    np.array([mhats, gapsN, gapsS]).transpose()[idx],
    header='mhat \t gapsN \t gapsS'
)
