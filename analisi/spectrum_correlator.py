#!/usr/bin/env python3

import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from tqdm import tqdm
import utils

N_RESAMPLE = 100
BLOCK_SIZE = 100

SKIP_THERM = 0

FIT_STOP_FRACTION = 1/3
REFIT_FRACTION = 5

PLOT_CORRELATION = False
PLOT_MODULO = 20
PLOT_CHI2NORM = False

plt.style.use('style.yml')


def fit_correlation(data, mhat, Ns, Nt, plot=False):
    energy = []
    chi2norm = []

    # Media e blocking delle correlazioni
    CorrN = np.mean(data, axis=0)

    # np.apply_along_axis(lambda x: utils.find_blocksize(x, 1, 1, params['mhat']), 0, data)
    CorrS = np.apply_along_axis(lambda x: utils.blocking(x, BLOCK_SIZE), 0, data)

    # Fit della correlazione a fisso impulso
    # def corr_fit(x, a, b):
    #     return np.exp(-a*x) / (2 * b)

    def corr_fit(x, a):
        return np.exp(-a*x) / (2 * a)

    # def corr_fit(x, a, b, x0):
    #     return np.cosh(a * (x - x0))/b

    fit_stop = int(FIT_STOP_FRACTION * CorrN.shape[1])

    for k_idx in range(CorrN.shape[0]):
        k = 2 * np.pi * k_idx / Ns
        yn = CorrN[k_idx]
        ys = CorrS[k_idx]

        par0 = np.sqrt(params['mhat']**2 + k**2)
        p0 = [par0]
        # p0 = [par0, par0]

        # fit preliminare
        opt, cov = curve_fit(
            corr_fit, range(fit_stop), yn[:fit_stop], sigma=ys[:fit_stop],
            absolute_sigma=True, p0=p0
        )

        # fit preciso
        # fit_stop = int(REFIT_FRACTION/opt[0])
        # opt, cov = curve_fit(
        #     corr_fit, np.arange(fit_stop), yn[:fit_stop], sigma=ys[:fit_stop],
        #     absolute_sigma=True, p0=p0
        # )

        chi2norm.append(
            utils.chisq(np.arange(fit_stop), yn[:fit_stop], ys[:fit_stop], corr_fit, opt) / (fit_stop - len(opt))
        )
        energy.append(opt[0])

        if plot and (k_idx % PLOT_MODULO) == 0:
            print(f'{k} {chi2norm[-1] *  (fit_stop - len(opt))} / {(fit_stop - len(opt))}')
            x_plot = np.linspace(0, len(CorrN[k_idx]), num=500)
            plt.plot(2*[fit_stop], [min(CorrN[k_idx]), max(CorrN[k_idx])], linestyle='dashed', color='green', linewidth=0.5)
            plt.plot(x_plot, corr_fit(x_plot, *opt), color='black', linewidth='0.5')
            # plt.plot(x_plot, corr_fit(x_plot, *p0), color='blue')
            plt.errorbar(range(len(CorrN[k_idx])), CorrN[k_idx], yerr=CorrS[k_idx], linestyle='', color='blue', marker='.', markersize=1)
            plt.title(rf'$ \hat m = {mhat:.3f}, N_s = {Ns}, N_t = {Nt}, k = {k:.3f} $')
            plt.xlabel(r'$ \tau $')
            plt.ylabel(r'$ \langle \tilde\phi(k, \tau) \tilde\phi(k, 0) \rangle $')
            plt.savefig(f'{sys.argv[2]}/corr-{mhat}-{k_idx}.pdf')
            plt.close()
            # plt.show()

    return energy, chi2norm


def fit_with_bootstrap(data, params, N_resample, block_size):
    energies = []

    slice_shape = data.shape[1:]

    N_blocks = int(len(data) / block_size)
    block_stop = N_blocks * block_size
    data_blocked = np.reshape(data[:block_stop], (N_blocks, block_size, *slice_shape))

    rng = np.random.default_rng()
    for _ in tqdm(range(N_resample), desc=f'mhat = {params["mhat"]} '):
        rints = rng.integers(0, N_blocks, size=N_blocks)
        resample = np.reshape(data_blocked[rints], (N_blocks * block_size, *slice_shape))

        e, _ = fit_correlation(
            resample,
            params['mhat'], params['Ns'], params['Nt'],
        )

        energies.append(e)

    energy_err = np.std(energies, axis=0, ddof=1)

    energy, chi2norm = fit_correlation(
        data,
        params['mhat'], params['Ns'], params['Nt'],
        plot=PLOT_CORRELATION
    )

    if PLOT_CHI2NORM:
        plt.plot(range(len(chi2norm)), chi2norm)

    print(f'mean chi^2/ndof = {np.mean(chi2norm)}')

    return energy, energy_err


for f in os.listdir(sys.argv[1]):
    filepath = os.path.join(sys.argv[1], f)

    params = utils.get_parameters(filepath)
    data = utils.load_data(filepath, discard_last=1)
    data = np.reshape(data, (len(data), params['Nt'], params['m_max']))
    data = np.swapaxes(data, 1, 2)  # 3D array, axes are t_MC, k, tau

    energy, energy_err = fit_with_bootstrap(
        data[SKIP_THERM:], params,
        N_RESAMPLE, BLOCK_SIZE
    )
    momentum = 2*np.pi * np.arange(params['m_max']) / params["Ns"]

    with open(filepath) as fp:
        first_line = fp.readline()

    header = first_line.strip('# ').strip('\n')
    np.savetxt(
        os.path.join(sys.argv[3], f'energy-{params["mhat"]}.txt'),
        np.array([momentum, energy, energy_err]).transpose(),
        header=header
    )

# plt.show()
