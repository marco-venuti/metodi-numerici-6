#!/usr/bin/env python3

import numpy as np
from subprocess import run
import threading
import queue
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--ignore-existing', '-i', action='store_true', help='')
parser.add_argument('--out', '-o', type=str,
                    dest='outfolder', action='store', default='out',
                    help='Destination folder in which to save simulation data')
args = parser.parse_args()

name = args.outfolder
beta = 10
r = 5
mhat = np.flip(np.linspace(0.2, 0.8, 10))

alpha = r * beta
Nt = np.unique(np.round(beta/mhat)).astype(int)
Ns = r * Nt
mhat = beta / Nt

q = queue.Queue()


def worker():
    while True:
        item = q.get()
        print('========================')
        print(item)
        print('========================')
        run(f'./build/mc <<EOF\n{item}\nEOF', shell=True)
        q.task_done()


os.makedirs(f'build/{name}', exist_ok=args.ignore_existing)

for mhat_, Nt_, Ns_ in zip(mhat, Nt, Ns):
    out = f'''
path:
  phi2: build/{name}/phi2-{mhat_}-{Ns_}-{Nt_}.txt
  spec: build/{name}/spec-{mhat_}-{Ns_}-{Nt_}.txt

phys:
  mhat: {mhat_}
  Nt: {Nt_}
  Ns: {Ns_}

sim:
  iterazioni: 5000
  saveStep: 10
  init: 0
  phi2Meas: True
  spectMeas: True
'''
    q.put(out)

for i in range(4):
    threading.Thread(target=worker, daemon=True).start()

q.join()
