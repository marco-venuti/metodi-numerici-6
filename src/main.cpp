#include <iostream>
#include <random>
#include <cmath>
#include <yaml-cpp/yaml.h>
#include <algorithm>

#include "pcg-cpp/include/pcg_random.hpp"

#define OVERRELAXATION_NUMBER 4
#define M_2PI 6.28318530717958647692

using namespace std;

YAML::Node get_config_stdin()
{
    istreambuf_iterator<char> begin(cin), end;
    string config_str(begin, end);
    // cout << config_str;
    return YAML::Load(config_str);
}

double F(double ** phi, unsigned deltaN, unsigned tau, unsigned Ns, unsigned Nt)
{
    double sum = 0;
    for (unsigned a = 0; a < Ns; a++)
        for (unsigned b = 0; b < Nt; b++)
            sum += phi[a][b] * (
                phi[(a + deltaN)%Ns][(b+tau)%Nt] + phi[(a - deltaN + Ns)%Ns][(b+tau)%Nt]
                );
    return sum/(2*Ns*Nt);
}

int main()
{
    YAML::Node config = get_config_stdin();

    string outPath_phi2     = config["path"]["phi2"].as<string>();
    string outPath_spectrum = config["path"]["spec"].as<string>();

    double   mhat = config["phys"]["mhat"].as<double>();
    unsigned Nt   = config["phys"]["Nt"].as<double>();
    unsigned Ns   = config["phys"]["Ns"].as<double>();

    unsigned iter      = config["sim"]["iterazioni"].as<unsigned>(); // tempo montecarlo totale/(Nt*Ns)
    unsigned save_step = config["sim"]["saveStep"].as<unsigned>(); // ogni quanto misurare
    double   init      = config["sim"]["init"].as<double>(); // valore con cui viene inizializzato il campo

    // Cosa misurare
    const bool phi2Meas  = config["sim"]["phi2Meas"].as<bool>();
    const bool spectMeas = config["sim"]["spectMeas"].as<bool>();

    // Inizializzo il RNG
    pcg_extras::seed_seq_from<random_device> seed_source;
    pcg32 gen(seed_source);

    // uniform_real_distribution<double>  dis01(0, 1);
    uniform_int_distribution<unsigned> disNs(0, Ns-1);
    uniform_int_distribution<unsigned> disNt(0, Nt-1);
    normal_distribution<double> disNormal(0, 1);

    // Calcolo chi sono i primi vicini
    unsigned * nn_s_r = new unsigned [Ns];
    unsigned * nn_s_l = new unsigned [Ns];
    unsigned * nn_t_u = new unsigned [Nt];
    unsigned * nn_t_d = new unsigned [Nt];

    for(unsigned p=0; p<Ns; p++)
        nn_s_r[p] = (p+1)%Ns;
    for(unsigned p=0; p<Ns; p++)
        nn_s_l[p] = (p-1+Ns)%Ns;
    for(unsigned p=0; p<Nt; p++)
        nn_t_u[p] = (p+1)%Nt;
    for(unsigned p=0; p<Nt; p++)
        nn_t_d[p] = (p-1+Nt)%Nt;

    // Inizializzo il campo
    double ** phi = new double * [Ns];
    for(unsigned i=0; i<Ns; i++) {
        phi[i] = new double [Nt];
        for(unsigned j=0; j<Nt; j++)
            phi[i][j] = init;
    }

    // Faccio il montecarlo
    double mhat2 = mhat*mhat;
    double sigma2 = 1/(mhat2 + 4);
    double sigma = sqrt(sigma2);

    unsigned ns, nt;
    unsigned N = Ns*Nt;
    double mean_field, sum;

    // Faccio il precaching dei coseni
    double ** Fcache = nullptr, ** cosCache = nullptr;

    unsigned m_max = Ns / M_2PI; // massimo valore dell'impulso k_max = 2*pi m_max / N_s = 1
    if (spectMeas) {
        // Cache per i valori delle correlazioni F(deltaN, tau)
        Fcache = new double*[Ns];
        for (unsigned deltaN = 0; deltaN < Ns; deltaN++)
            Fcache[deltaN] = new double[Nt];

        // Cache per i valori del prefattore 2 (1 - deltaN/Ns) cos(2 pi m deltaN / Ns) per la trasformata
        cosCache = new double*[m_max];
        for (unsigned m = 0; m < m_max; m++){
            cosCache[m] = new double[Ns - 1];
            for (unsigned deltaN = 1; deltaN < Ns; deltaN++)
                cosCache[m][deltaN - 1] = cos(deltaN * M_2PI * m / Ns) * 2. * (1 - (double) deltaN / Ns);
        }
    }

    // Preparo l'output
    FILE * out_file_phi2 = nullptr, *out_file_spectrum = nullptr;

    if (phi2Meas)
    {
        out_file_phi2 = fopen(outPath_phi2.c_str(), "w");
        fprintf(out_file_phi2, "# mhat: %f, Nt: %d, Ns: %d, save_step: %d \n", mhat, Nt, Ns, save_step);
    }

    if (spectMeas)
    {
        out_file_spectrum = fopen(outPath_spectrum.c_str(), "w");
        fprintf(out_file_spectrum, "# mhat: %f, Nt: %d, Ns: %d, save_step: %d, m_max: %d \n", mhat, Nt, Ns, save_step, m_max);
    }

    for (unsigned i = 0; i < iter; i++)
    {
        for(unsigned j = 0; j < N; j++)
        {
            // HEAT BATH

            // Scelgo il sito da aggiornare
            ns = disNs(gen);
            nt = disNt(gen);

            mean_field = sigma2 * (phi[nn_s_r[ns]][nt] + phi[nn_s_l[ns]][nt] + phi[ns][nn_t_u[nt]] + phi[ns][nn_t_d[nt]]);
            phi[ns][nt] = disNormal(gen)*sigma + mean_field;

            // OVER-RELAXATION
            for (unsigned _ = 0; _ < OVERRELAXATION_NUMBER; _++)
            {
                ns = disNs(gen);
                nt = disNt(gen);
                mean_field = sigma2 * (phi[nn_s_r[ns]][nt] + phi[nn_s_l[ns]][nt] + phi[ns][nn_t_u[nt]] + phi[ns][nn_t_d[nt]]);
                phi[ns][nt] = 2*mean_field - phi[ns][nt];
            }
        }


        if (!(i%save_step)) {
            // Guardo la media di phi^2
            if (phi2Meas) {
                sum = 0;
                for (unsigned a=0; a<Ns; a++)
                    for(unsigned b=0; b<Nt; b++)
                        sum += pow(phi[a][b], 2);
                fprintf(out_file_phi2, "%f\n", sum / (Ns * Nt));
            }

            // Spettro della teoria
            // A ogni tempo montecarlo in cui misuro stampo una riga
            // La riga è l'array dei correlatori c(tau, k) flattened con 0 <= tau < Nt-1 e 0 <= k < 1
            if (spectMeas) {
                for (unsigned tau = 0; tau < Nt; tau++) {
                    // Genera i valori di F(tau, deltaN) che sono indipendenti dall'impulso
                    for (unsigned deltaN = 0; deltaN < Ns; deltaN++)
                        Fcache[deltaN][tau] = F(phi, deltaN, tau, Ns, Nt);

                    // Itera sull'impulso, k = 2 * pi * m / N_s
                    for (unsigned m = 0; m < m_max; m++) {
                        sum = Fcache[0][tau];
                        for (unsigned deltaN = 1; deltaN < Ns; deltaN++)
                            sum += cosCache[m][deltaN - 1] * Fcache[deltaN][tau];
                        fprintf(out_file_spectrum, "%f ", sum);
                    }
                }
                fprintf(out_file_spectrum, "\n");
            }
        }
    }


    // Faccio pulizia
    delete [] nn_s_l;
    delete [] nn_s_r;
    delete [] nn_t_d;
    delete [] nn_t_u;
    for (unsigned i = 0; i < Ns; i++)
        delete [] phi[i];

    delete [] phi;

    if (phi2Meas)
        fclose(out_file_phi2);

    if (spectMeas) {
        fclose(out_file_spectrum);

        for (unsigned deltaN = 0; deltaN < Ns; deltaN++)
            delete [] Fcache[deltaN];
        delete [] Fcache;

        for (unsigned m = 0; m < m_max; m++)
            delete [] cosCache[m];
        delete [] cosCache;
    }

    return 0;
}
