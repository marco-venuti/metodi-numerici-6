\documentclass{article}

\usepackage[sectionClearpage=false,images=true]{npStyle}
\usepackage{fullpage}

\usepackage[separate-uncertainty=true]{siunitx}
\newcommand{\red}[1]{\textcolor{red}{#1}}

\title{
    Modulo 6 \\
    \Large{PIMC per una teoria di campo scalare libera}
}
\author{Alessandro Piazza \and Porciani Niccolò \and Marco Venuti}

\begin{document}

\maketitle
\tableofcontents

\section{Campo scalare libero}
\subsection{Discretizzazione}

Consideriamo la teoria di campo quantistica in \( 1 + 1 \) dimensioni con lagrangiana
\begin{equation}
    \mathscr{L}[\phi, \partial_\mu \phi] = \frac{1}{2} \del{\partial_\mu \phi\, \partial^\mu \phi - m^2 \phi^2}
\end{equation}
e consideriamone la funzione di partizione data dal path integral, in metrica euclidea
\begin{equation}
    Z_E[\beta] = \int\displaylimits_{\phi(\x,0) = \phi(\x,\beta \hbar)}\hspace{-4ex} \mathcal{D}[\phi(x)] e^{-S_E[\phi]}
\end{equation}
dove
\begin{equation}
    S_E[\phi] = \int_0^{\beta\hbar} \dif{\tau} \int_V \diff{\x} \mathscr{L}_E[\phi, \partial_\mu \phi] = \int_0^{\beta\hbar} \dif{\tau} \int_V \diff{\x} \frac{1}{2} \del{\partial_\mu \phi \partial^\mu \phi + m^2 \phi^2}
\end{equation}
Possiamo discretizzare il path integral su un reticolo ipercubico \( R \) di spaziatura \( a \), di dimensione \( N_t \times N_s \), con condizioni al bordo periodiche. L'azione discretizzata è
\begin{equation}
    S_E = \sum_{\vec{n} \in R} \frac{1}{2}\del{\sum_{\mu = 1,2} \del{\phi(\vec{n} + \vec{\mu}) - \phi(\vec{n})}^2 + \hat{m}^2\phi^2}
\end{equation}
dove \( \hat{m} = ma \) e con \( \vec{n} + \vec{\mu} \) indichiamo il vettore \( \vec{n} \) in cui la coordinata \( \mu \)-esima è aumentata di 1.

\subsection{Misura di \( \phi^2 \)}
Come studio preliminare del sistema abbiamo considerato un'osservabile semplice quale \( \phi^{2} \). Nella teoria continua il suo valore atteso sul vuoto è
\begin{equation}
    \label{eq:phi2-continuo}
    \braket{0 | \phi^2(x) | 0} = \int \frac{\diff{p}}{2\pi} \frac{1}{2E_{p}}
\end{equation}
che è chiaramente divergente. Tuttavia, la discretizzazione fornisce un cut-off ultravioletto agli impulsi \( \Lambda \sim 1 / a \propto 1/\hat{m} \); in particolare possiamo calcolare esattamente la~\eqref{eq:phi2-continuo} mettendo un hard cut-off
\begin{equation}
    \braket{0 | \phi^2(x) | 0} =  \int_{-\Lambda}^{\Lambda} \frac{\diff{p}}{4\pi} \frac{1}{\sqrt{m^{2} + p^{2}}} = \frac{1}{2\pi} \log\left(\frac{\Lambda}{m} + \sqrt{1 + \frac{\Lambda^{2}}{m^{2}}}\right)
\end{equation}
Quindi se assumiamo \( \Lambda = c/a = c m / \hat{m} \) e consideriamo il limite \( \hat{m} \to 0 \) abbiamo un'espansione del tipo
\begin{equation}
    \braket{0 | \phi^2(x) | 0} = -\frac{1}{2\pi} \log{\hat{m}} + A + \mathcal{O}(\hat{m}^{2})
\end{equation}
dove \( C \) è una costante.

\subsection{Misura dello spettro}

Dall'andamento asintotico dei correlatori temporali sul vuoto di appropriate osservabili possiamo ricavare informazioni sullo spettro della teoria. In particolare consideriamo
\begin{equation}
    C(p, q,\tau) = \braket{0 | \int \diff{x} \phi(x,\tau) e^{ipx} \int \diff{y} \phi(y,0) e^{-iqy} | 0} = 2\pi \frac{e^{-E_p \tau}}{2E_p} \delta(p-q)
\end{equation}
Questo correlatore ha il significativo vantaggio di selezionare un unico impulso e quindi un'unica energia, e la dipendenza asintotica è quindi data da un unico termine esponenziale. Da un fit esponenziale in \( \tau \) possiamo allora ricavare una stima di \( E_k \), e ripetendo la procedura estrarre quindi la relazione di dispersione.

Numericamente dovremo considerare la versione discreta dei correlatori. Per comodità lavoriamo su un volume \( [-L,L] \) e discretizziamo con punti in \( x = na \), \( n = -M, \dots, 0, \dots, M - 1 \), e \( \tau = \ell a \), \( \ell = 0, \dots, N_t - 1 \). L'impulso si adimensionalizza come \( p = k/a \) e i possibili valori saranno \( k = \pi s / M \), con \( s = 0, \dots, 2M - 1 \). Le trasformate di Fourier si discretizzano come
\begin{equation}
    \int \diff{x} \phi(x,\tau) e^{ipx}
    = \lim\limits_{L \to \infty} \int_{-L}^{+L} \diff{x} \phi(x,\tau) e^{ipx}\\
    = -\lim_{L\to +\infty} \lim\limits_{\substack{a \to 0\\ Ma = L}} a \sum_{n = 0}^{2M - 1} \hat{\phi}\del{n, \ell} e^{ 2\pi i \frac{n s}{2M}}
\end{equation}
dove si è usata l'invarianza sotto traslazioni. Abbiamo allora che, posto \( p = k_p /a = \frac{\pi}{N_s} s_p \) e analogamente per \( q \),
\begin{gather}
    C\del{p,q,\tau} = \lim_{V\to +\infty} \lim\limits_{\substack{a \to \infty\\ N_{s}a = V}} \Braket{\sum_{n,n' = 0}^{N_{s} - 1} \hat{\phi}(n,\ell) \hat{\phi}(n',0) e^{\frac{2\pi i}{N_{s}}(ns_p-n's_q)}}
\end{gather}
ove \( N_s = 2M \) per tornare alla discretizzazione usuale.

Viceversa per il lato destro della~\eqref{eq:phi2-continuo} abbiamo
\begin{equation}
    E_p = \sqrt{m^2 + p^2} = \frac{1}{a}\sqrt{\hat{m}^2 + k^2} \qquad
    k = \frac{2\pi}{N_s} s
\end{equation}
e la delta si discretizza come
\begin{equation}
    \delta(p-q)
    = \lim\limits_{L \to \infty} \int_{-L}^{+L} \frac{\diff{x}}{2\pi} e^{i(p-q)x}\\
    = \lim_{V\to +\infty}\lim\limits_{\substack{a\to 0\\ M a = V}} \frac{a}{2\pi} \sum_{n = -M}^{M - 1} e^{i \pi(s_p - s_q) n / M}\\
    = \lim_{V\to +\infty}\lim\limits_{\substack{a\to 0\\ M a = V}} \frac{a}{2\pi} 2M \delta_{s_p,s_q}
\end{equation}
da cui
\begin{equation}
    2\pi \frac{e^{-E_p \tau}}{2E_p} \delta(p-q) = \lim_{V \to + \infty}\lim\limits_{\substack{a \to 0 \\ N_{s} a = V}} a^2\,N_{s}\, \frac{e^{- \sqrt{\hat{m}^2 + k^2} \tau}}{2\sqrt{\hat{m}^2 + k^2}}
\end{equation}
Uguagliando e semplificando la relazione discretizzata sarà allora
\begin{equation}\label{eq:correl_discreto}
    \bar{C}(s,\ell) = \frac{1}{N_s}\Braket{\sum_{n,n' = 0}^{N_s - 1} \hat{\phi}(n,\ell) \hat{\phi}(n',0) e^{i\frac{2\pi}{N_s}s(n-n')}} = \frac{e^{- \sqrt{\hat{m}^2 + k^2} \tau}}{2\sqrt{\hat{m}^2 + k^2}} \qquad (\hat m \to 0).
\end{equation}

Per ogni valore di \( \hat{m} \) si ricava quindi una dispersione \( E(k,\hat{m}) \), che abbiamo scelto di fittare con legge
\begin{equation}
    E(k, \hat{m}) = \sqrt{\hat m^2\ped{fit} + k^2}
\end{equation}
per ricavare il valore del gap della teoria. Tale procedura andrà chiaramente ripetuta a vari valori di \( \hat{m} \), e si dovrà considerare il limite continuo \( \hat{m} \to 0 \).

Nella pratica il lato sinistro dell'equazione~\eqref{eq:correl_discreto} è stato valutato numericamente nella seguente forma
\begin{equation}
    \bar{C}(s,\ell) = F(0,\ell) + \frac{2}{N_s} \sum_{\Delta n = 1}^{N_s - 1} \cos\del{\frac{2\pi}{N_s} s \Delta n} F(\Delta n, \ell) \del{N_s - \Delta n}
\end{equation}
dove
\begin{equation}
    F(n - n', \ell) = \braket{\phi(n,\ell) \phi(n', 0)}
\end{equation}
è stata calcolata mediando su tutte le possibili coppie del reticolo, a tempo Monte Carlo fissato, per cui la distanza spaziale fosse \( \pm(n - n') \) e quella temporale fosse \( \ell \), sfruttando l'invarianza sotto traslazioni e sotto parità spaziale.

\section{Implementazione}
Per simulare il sistema a temperatura finita possiamo usare algoritmi di simulazione locale; nel nostro caso abbiamo utilizzato una mossa di \emph{heat-bath}, cioè un'estrazione di \( \phi(\vec{n}) \) (per un \( \vec{n} \) scelto casualmente in modo uniforme) con distribuzione gaussiana
\begin{equation}
    P(\phi(\vec{n})) \propto \exp\left[-\frac{\hat{m}^{2} + 4}{2}{\left(\phi(\vec{n}) - \frac{f(\vec{n})}{\hat{m}^{2} + 4}\right)}^{2}\right] \qquad
    f(\vec{n}) = \sum_{\mu=1,2} (\phi(\vec{n} + \vec{\mu}) + \phi(\vec{n} - \vec{\mu}))
\end{equation}
seguita da 4 mosse di \emph{over-relaxation} su altrettanti siti scelti casualmente, i.e.
\begin{equation}
    \phi(\vec{n}) \to 2 \frac{f(\vec{n})}{\hat{m}^{2} + 4} - \phi(\vec{n}).
\end{equation}
Nel seguito intenderemo che un singolo tempo Monte Carlo consiste nella ripetizione di queste mosse in numero pari al numero di siti del reticolo \( N_{s} \cdot N_{t} \).\\

Tale algoritmo è stato implementato in \texttt{C++} ed è disponibile al repository \href{https://gitlab.com/marco-venuti/metodi-numerici-6}{metodi-numerici-6} (si veda in particolare \texttt{src/main.cpp}), dove sono anche presenti gli script utilizzati per l'analisi delle simulazioni.\\

In tutte le simulazioni abbiamo fissato \( \beta m = N_t \hat{m} = 10 \), valore che abbiamo considerato come sufficientemente grande per ignorare effetti di temperatura finita, in quanto questi sono soppressi esponenzialmente in \( \beta m \). Il parametro che controlla il limite di volume infinito è invece \( \alpha = Lm = N_s \hat{m} \); si vedano le sezioni specifiche per una discussione di come è stato scelto questo parametro.

\section{Analisi}

\subsection{Divergenza di \( \phi^{2} \)}
Abbiamo effettuato una sweep di 10 valori \( \hat{m} \in [0.01, 0.2] \), facendo 50000 step Monte Carlo con misure ogni 10. La dimensione del reticolo è stata scalata prendendo \( N_{s} = N_{t} = \beta / \hat{m} \), da cui \( \alpha = 10 \), valore che abbiamo ritenuto ragionevole in quanto abbiamo verificato la stabilità del risultato per qualche valore differente di \( \alpha \).

Il valore di \( \phi^{2} \) al singolo tempo Monte Carlo è stato mediato su tutto il reticolo, sfruttando l'invarianza sotto traslazioni spazio-temporali. L'errore associato alle stime di \( \braket{\phi^{2}} \) è stato ottenuto tramite \emph{blocking} con dimensione del blocco pari a \( 200 \) (valore per cui tutte le stime dell'errore raggiungono un plateau). Si riporta un grafico esplicativo in Figura~\ref{fig:phi2-blocking}.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.42]{imgs/phi2-blocking-0.052}
    \caption{\label{fig:phi2-blocking}blocking per \( \braket{\phi^{2}} \) per un valore caratteristico di \( \hat{m} \).}
\end{figure}

Si è effettuato un fit lineare in scala semi-logaritmica (\( f(x) = a + b\, x \)) di \( \braket{\phi^{2}} \) in funzione di \( \hat{m} \), ottenendo un valore per la costante moltiplicativa compatibile entro 2 deviazioni standard con quello atteso di \( -1/2\pi \)
\begin{equation}
    -2\pi b = \num{1.007 +- 0.004} \qquad \chi^{2} / \mathrm{ndof} = 6.8 / 8
\end{equation}
In Figura~\ref{fig:phi2} si riportano le misure e la curva di \emph{best fit}.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.77]{imgs/phi2}
    \caption{\label{fig:phi2}divergenza logaritmica di \( \braket{\phi^{2}} \), con sovraimposta la retta di best fit.}
\end{figure}

\subsection{Spettro della teoria}

\begin{figure}[t]
    \centering
    \includegraphics[scale=0.85]{imgs/dispersion-full}
    \caption{\label{fig:dispersione-discreta}curva di dispersione completa come ottenuta dal Monte Carlo per \( \hat m = 0.5 \). In rosso la curva di best fit ottenuta con modello iperbolico usando solo i valori per \( k < 0.4 \).}
\end{figure}

Ci aspettiamo che la dispersione valutata numericamente sia in buon accordo con quella attesa nel limite continuo solo per \( k \ll 1 \)\footnote{
    Un plot della curva di dispersione ottenuta dalle simulazioni per tutti i valori di \( k \) è riportato in Figura~\ref{fig:dispersione-discreta}. È chiaramente visibile la deviazione dovuta alla discretizzazione del reticolo.
}, e nel fit corrispondente è quindi necessario porre un cutoff \( k_0 \) che limiterà il numero di punti fittati a
\begin{equation}
    N\ped{punti} = \left\lfloor \frac{N_s}{2\pi} k_0  \right\rfloor = \left\lfloor \frac{\alpha}{2\pi\hat{m}} k_0  \right\rfloor
\end{equation}
Dovremo quindi richiedere anche che \( \alpha \) sia sufficientemente grande da avere un numero ragionevole di punti per il massimo valore di \( \hat{m} \), ma così facendo in corrispondenza del minimo \( \hat{m} \) il valore di \( N_s \) diventa troppo grande e quindi computazionalmente intrattabile. \\

Abbiamo allora scelto di eseguire due set di simulazioni, entrambi con \( 10^{5} \) step e misure ogni 10, rispettivamente con
\begin{itemize}
    \item 5 valori di \( \hat{m} \) equispaziati in \( \sbr{0.05, 0.2} \), con \( \alpha = 15 \);
    \item 9 valori di \( \hat{m} \) equispaziati in \( \sbr{0.28, 1} \), con \( \alpha \) variati in modo da tenere costante \( N\ped{punti} = 7 \) a \( k_0 = 0.3 \).
\end{itemize}
Nel singolo campione le incertezze sui valori dei correlatori \( \bar{C}(s,\ell) \) sono state stimate tramite blocking con dimensione del blocco pari a 100, scelta in modo tale che l'errore raggiungesse un plateau per tutti i possibili valori di \( s \) ed \( \ell \).

\paragraph*{Correlatori} Per ogni \( \hat{m} \) si è quindi innanzi tutto eseguito un fit esponenziale su \( \bar{C}(s,\ell) \) a fisso \( s \), con modello
\begin{equation}\label{eq:corr-modello}
    f(\ell) = \frac{e^{- c \ell}}{2c}
\end{equation}
dove \( c \) risulterà essere funzione di \( \hat{m} \) ed \( s \), e rappresenta la dispersione della teoria. \\
Abbiamo scelto di considerare nel fit i dati fino a \( \ell = N_{t}/3 \) per evitare che il fit fosse condizionato dalla finitezza e periodicità del tempo euclideo che comporta la presenza di una risalita esponenziale.

I dati utilizzati in questo fit sono però correlati, in quanto i valori dei correlatori \( C(s,\ell) \) sono stimati dalla stessa storia Monte Carlo.
Dato che la routine di fit utilizzata (\texttt{curve\_fit} di \texttt{scipy}) presume che i dati siano scorrelati nel determinare l'incertezza sui parametri, per stimare quest'ultima si è allora eseguito un \emph{bootstrap} (con \emph{binning} a blocchi di 100), calcolando i correlatori da 100 resample della storia Monte Carlo, eseguendo i fit corrispondenti e prendendo come incertezza sul parametro di best-fit (il cui valore centrale è ottenuto dal sample originario) la deviazione standard dei risultati ottenuti dai resampling. Per i dettagli si veda \texttt{analisi/spectrum\_correlators.py}. \\

I \( \chi^{2} \) dei fit presentano notevole variabilità in funzione di \( \hat m \) e \( k \). Notiamo che:
\begin{itemize}
    \item Nonostante la procedura di \emph{bootstrap}, i singoli fit sono comunque eseguiti su dati correlati e quindi, dato che i \( \chi^{2} \) tengono conto solo delle varianze dei \( \bar C (s, \ell) \), questi risultano più piccoli di quanto ci si attenderebbe da un buon fit.

    Effettivamente questo si riscontra almeno per gli \( \hat m \) e i \( k \) più piccoli.
    \item Il \( \chi^{2} \) cresce all'aumentare di \( k \) e di \( \hat m \). Una possibilità, avvalorata da quanto si vede ad esempio dalla Figura~\ref{fig:fit-corr-schifo} è che il modello~\eqref{eq:corr-modello} non sia più valido per grandi \( k \) e/o \( \hat{m} \).
    In effetti, l'andamento esponenziale del correlatore rimane valido anche per la teoria di Klein-Gordon con spazio discretizzato, ma il tempo deve essere continuo. Questa ultima condizione si può così imporre
    \begin{equation}
        a_{t} \ll \frac{1}{\omega_{s}} \quad \forall s
    \end{equation}
    dove \( \omega_{s} = \sqrt{m^{2} + \frac{4}{a_{s}^{2}} \sin^{2}\del{\frac{\pi s}{N_{s}}}} \) sono le frequenze di Klein-Gordon discreto. Otteniamo quindi
    \begin{equation}
        \hat m^{2} + 4 \sin^{2}\del{\frac{\pi s}{N_{s}}} \ll 1
    \end{equation}
    da cui deduciamo che non solo \( \hat m \) deve essere piccolo, ma che se \( s \) è troppo grande questa condizione non può essere soddisfatta, da cui il discostamento dall'andamento esponenziale previsto dalla teoria a tempo continuo.

    In Figura~\ref{fig:fit-corr-decente} invece il fit risulta migliore, essendo \( s = 0 \) e \( \hat m \ll 1 \).
\end{itemize}

\begin{figure}[h!]
    \centering
    \subfloat[\label{fig:fit-corr-schifo}\( \chi^{2}/\mathrm{ndof} = 493/3 \)]{\includegraphics[scale=0.56]{imgs/corr-0.714286-20}}
    \subfloat[\label{fig:fit-corr-decente}\( \chi^{2}/\mathrm{ndof} = 17/36 \)]{\includegraphics[scale=0.56]{imgs/corr-0.09009-0}}
    \caption{\label{fig:fit-corr}fit esponenziale dei correlatori per due diversi valori della coppia \( (\hat m, k) \). Il massimo \( k \) considerato nei fit è demarcato dalla linea verde tratteggiata.}
\end{figure}


\paragraph*{Dispersione} I parametri di best fit \( c(\hat{m},s) \) così ottenuti sono poi stati fittati, separatamente per ogni \( \hat m \), in funzione di \( k = \frac{2\pi}{N_s} s \) con modello
\begin{equation}
    f(k) = \sqrt{\hat{m}^2\ped{fit} + k^2}
\end{equation}
dove il range di fit è stato ristretto a \( k < k_0 = 0.3 \), fissato in modo che sia \( k_{0} \ll 1 \) e che il fit fosse ragionevolmente stabile al variare del range.
In Figura~\ref{fig:dispersione} si riportano alcuni esempi di fit della relazione di dispersione, con i relativi \( \chi^{2} \), che risultano in accordo con quanto atteso, sebbene per gli \( \hat m \) più grandi risulti leggermente superiore a quanto atteso, coerentemente con il fatto che siano più rilevanti gli errori di discretizzazione. Il fit delle relazioni di dispersione è fatto in \texttt{analisi/spectrum\_energy.py}.

\begin{figure}[h!]
    \centering
    \subfloat[\( \hat m = 0.05 \), \( \chi^{2}/\mathrm{ndof} = 10.8/14 \)]{\includegraphics[scale=0.56]{imgs/energy-0.05}}
    \subfloat[\( \hat m = 0.09 \), \( \chi^{2}/\mathrm{ndof} = 7.5/7 \)]{\includegraphics[scale=0.56]{imgs/energy-0.09009}} \\
    \subfloat[\( \hat m = 0.29 \), \( \chi^{2}/\mathrm{ndof} = 14.7/8  \)]{\includegraphics[scale=0.56]{imgs/energy-0.285714}}
    \subfloat[\( \hat m = 0.71 \), \( \chi^{2}/\mathrm{ndof} = 14.2/8  \)]{\includegraphics[scale=0.56]{imgs/energy-0.714286}} \\

    \caption{\label{fig:dispersione}esempi di fit della relazione di dispersione, per vari \( \hat m \). In rosso la curva di best-fit e in verde il cutoff \( k_{0} = 0.3 \).}
\end{figure}


\paragraph*{Estrapolazione} Si può infine procedere ad un'estrapolazione al limite continuo \( \hat{m} \to 0 \) del valore del gap \( \hat m \ped{fit} \), diviso per il parametro di simulazione \( \hat m \) in modo da ottenere nel limite una quantità finita.
Per l'estrapolazione abbiamo assunto correzioni quadratiche rispetto al valore asintotico, ed abbiamo quindi eseguito un fit con modello
\begin{equation}
    f(\hat m) = a + b \hat m ^{2}
\end{equation}
sui dati \( \hat m \ped{fit}(\hat m) / \hat m \). Ci attendiamo naturalmente che \( a = 1 \).
I risultati di questo fit, riportati in Figura~\ref{fit:estrapolazione} sono
\begin{equation}
    a = \num{0.9997 +- 0.0008} \qquad
    b = \num{0.077 +- 0.002} \qquad
    \chi^{2}/\mathrm{ndof} = 16/12
\end{equation}
compatibile con quanto atteso.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=1]{imgs/spectrum-fit.pdf}
    \caption{\label{fit:estrapolazione}valori del gap ottenuti dalle simulazioni in funzione di \( \hat m \), e relativo fit per l'estrapolazione nel limite del continuo.}
\end{figure}


\end{document}